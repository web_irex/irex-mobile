class User {
  late final String id;

  late final String nom_utilisateur;
  late final String mot_de_passe;

  late final String token;

  late final String noms;
  late final String prenoms;
  // late final String sexe;
  late final String email;
  late final String tel;
  /* late final String situation_S_P;
  late final String centre_d_interet;
  late final String attentes;
  late final String file_CV;
 */
  User(
      this.id,
      // this.nom_utilisateur,
      // this.mot_de_passe,
      // this.token,
      this.noms,
      this.prenoms,
      // this.sexe,
      this.email,
      this.tel,
      /* this.situation_S_P,
      this.centre_d_interet,
      this.attentes, */
      /* this.file_CV */);
  //  méthode pour convertir l'objet User en une carte (map) JSON
  Map<String, dynamic> toJson() {
    return {
      'nom': noms,
      'prenom': prenoms,
      'email': email,
      'tel': tel,
    };
  }

  User.empty();
}
