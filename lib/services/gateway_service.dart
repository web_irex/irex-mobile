import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';
import 'package:space_irex_mobile/model/User.dart';

import '../encrypt/crypt.dart';
import '../model/Reponse.dart';
import '../model/Constantes.dart';
class GatewayService {
  final String gatewayUrl;

  // Ajoutez un constructeur qui prend une URL en paramètre
  GatewayService(this.gatewayUrl, this.client);

  // Ajoutez un constructeur nommé pour créer une instance avec une URL par défaut
  GatewayService.withDefaultUrl(this.client) : gatewayUrl = GATEWAY_URL;

  // Ajoutez un client HTTP en tant que paramètre
  final http.Client client;

  // Ajoutez un constructeur qui prend un client en paramètre (pour les tests)
  GatewayService.withClient(this.client, this.gatewayUrl);

  var logger = Logger();

  Future<Reponse?>? login(String usrname, String password) async {
    String url = '$gatewayUrl$LOGIN_ENDPOINT';
    final uri = Uri.parse(url);
    Map<String, String> headers = {
      'Content-Type': 'application/json; charset=UTF-8',
    };
    Map<String, String> data = {
      'username' : decrypt(usrname),
      'password' : decrypt(password),
    };

    http.Response response = await http.post(uri , headers: headers, body: jsonEncode(data));

    print(response.body);
    print(response.statusCode);


    if(response.statusCode == 200){
      dynamic token = json.decode(response.body)['access_token'].toString();

      Reponse okReponse = Reponse(token, response.statusCode);

      return okReponse;
    }else if(response.statusCode == 401){
      Reponse noAuthReponse = Reponse("NOT_AUTHORIZED", response.statusCode);
      return noAuthReponse;
    }else if(response.statusCode == 400){
      Reponse verifReponse = Reponse("NEED_EMAIL_VERIFICATION", response.statusCode);
      return verifReponse;
    }else{
      throw("Server Error");
    }
  }

  Future<Reponse?>? register(User user) async {
    try {
      final response = await http.post(
          Uri.parse('$gatewayUrl$REGISTER_ENDPOINT'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(user.toJson())
      );

      if (response.statusCode == 201) {
        logger.log(Level.info, 'Gateway call successful');
        logger.log(Level.info, response.body);


        Reponse okReponse = Reponse("User successful added", response.statusCode);

        return okReponse;
      } else if(response.statusCode == 409){

        Reponse alraedyReponse = Reponse("user already exist", response.statusCode);

        return alraedyReponse;
      }else {
        print('Gateway call failed with status code: ${response.statusCode}');
        print(response.body);
        throw("Server Error");
      }
    } catch (e) {
      throw('Error calling Gateway: $e');
    }
  }
}