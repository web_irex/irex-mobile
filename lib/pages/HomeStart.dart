import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:space_irex_mobile/model/User.dart';

class HomeStartPage extends StatefulWidget {
  const HomeStartPage({super.key, required this.user});

  final User user;

  @override
  State<HomeStartPage> createState() => _HomeStartPageState();
}

class _HomeStartPageState extends State<HomeStartPage> {


  DateTime? currentBackPressTime;

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
              automaticallyImplyLeading: false,
            title: Text('IREX App default page'),
          ),
          body: Center(
              child: Text("Welcome to IREX\n")
          ),

        ),
        onWillPop: (){
          DateTime now = DateTime.now();
          if(currentBackPressTime == null || now.difference(currentBackPressTime!)>Duration(seconds: 2)){
            currentBackPressTime=now;
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.transparent, content: Text("Tap again to Exit",textAlign: TextAlign.center, style: TextStyle(color: Colors.black),)));
            return Future.value(false);
          }else{
            SystemNavigator.pop();
            //Navigator.pop(context,false);
            return Future.value(false);}
        });


  }
}
