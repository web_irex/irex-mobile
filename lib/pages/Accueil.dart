import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:space_irex_mobile/classes/language.dart';
import 'package:space_irex_mobile/model/languageProvider.dart';
import 'package:space_irex_mobile/widget/Bouton.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:space_irex_mobile/pages/Login.dart';
import 'package:space_irex_mobile/pages/Register.dart';

import 'package:openvpn_flutter/openvpn_flutter.dart';
import 'package:flutter/services.dart' show rootBundle;
import '../vpn/vpnEnv.dart';

class AccueilPage extends StatefulWidget {
  const AccueilPage({super.key, required this.title});

  final String title;

  @override
  State<AccueilPage> createState() => _AccueilPageState();
}

class _AccueilPageState extends State<AccueilPage> {

  String configText = 'Empty';


  late OpenVPN engine;
  VpnStatus? status;
  VPNStage? stage;
  @override
  void initState() {
    engine = OpenVPN(
      onVpnStatusChanged: (data) {
        setState(() {
          status = data;
        });
      },
      onVpnStageChanged: (data, raw) {
        setState(() {
          stage = data;
        });
      },
    );

    engine.initialize(
      groupIdentifier: "irex.space.vpn",
      providerBundleIdentifier:
      "irex.space.openvpn.vpn",
      localizedDescription: "VPN by IREX",
      lastStage: (stage) {
        setState(() {
          this.stage = stage;
        });
      },
      lastStatus: (status) {
        setState(() {
          this.status = status;
        });
      },
    );
    super.initState();
  }

  Future<void> initPlatformState() async {
    engine.connect(
        configText,
        contry,
        username: defaultVpnUsername,
        password: defaultVpnPassword,
        certIsRequired: true);
    if (!mounted) return;
    vpnIsConnected = true;
  }


  bool vpnIsConnected = false;

  void seConnecter() {
    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
      return LoginPage(title: "title");
    }));
  }

  void _changeLanguage(Language language) {
    final languageProvider =
        Provider.of<LanguageProvider>(context, listen: false);
    final newLocale = Locale(language.languageCode, language.countryCode);
    languageProvider.setLocale(newLocale);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Stack(
        clipBehavior: Clip.none,
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.transparent,
            child: Ink.image(
              image: AssetImage("images/acc1.jpg"),
              fit: BoxFit.cover,
              width: MediaQuery.of(context).size.height,
              height: MediaQuery.of(context).size.width,
              child: InkWell(
                onTap: () {},
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: DropdownButton(
              onChanged: (Language? language) {
                if (language != null) {
                  _changeLanguage(language);
                }
              },
              underline: SizedBox(),
              icon: const Icon(
                Icons.language,
                color: Colors.white,
              ),
              items: Language.languageList()
                  .map<DropdownMenuItem<Language>>((lang) => DropdownMenuItem(
                        value: lang,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[Text(lang.name), Text(lang.flag)],
                        ),
                      ))
                  .toList(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30,top: 10),
            child: IconButton(
                onPressed: (){
                  setState(() {
                    if(vpnIsConnected){
                      vpnDisconnect();
                    }else{
                      vpnConnect();
                    }
                  });
                },
                icon: Icon(vpnIsConnected?Icons.vpn_key:Icons.vpn_key_off,color: Colors.white,)),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height: MediaQuery.of(context).size.height / 2,
              ),
              buildButton(AppLocalizations.of(context)!.loginButton,
                  CupertinoColors.systemBlue, Colors.white, seConnecter),
              RichText(
                text: TextSpan(
                    text: AppLocalizations.of(context)!.ifNoAccount,
                    style: const TextStyle(color: Colors.white, fontSize: 14),
                    children: [
                      TextSpan(
                        text: AppLocalizations.of(context)!.registerLink,
                        style: TextStyle(
                            color: Colors.orange,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            Navigator.push(context, MaterialPageRoute(
                                builder: (BuildContext context) {
                              return RegisterPage(title: "title");
                            }));
                          },
                      ),
                    ]),
              ),
            ],
          )
        ],
      )),
    );
  }

  void vpnConnect()async{
    String response;
    response = await rootBundle.loadString(config);
    setState(() {
      configText = response;
    });


    initPlatformState();
  }

  void vpnDisconnect(){
    engine.disconnect();
    vpnIsConnected = false;
  }

}
